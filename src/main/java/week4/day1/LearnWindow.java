package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWindow {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		driver.findElementByXPath("(//span[text()='AGENT LOGIN'])[1]").click();
		Thread.sleep(2000);
		driver.findElementByLinkText("Contact Us").click();
		Set<String> number = driver.getWindowHandles();
		System.out.println("Number of windows: "+number.size());
		List<String> tbb=new ArrayList<String>();
		tbb.addAll(number);
		String count = tbb.get(2);
		driver.switchTo().window(count);
		String tit=driver.getTitle();
		System.out.println("Title of second window: "+tit);
		String us = driver.getCurrentUrl();
		System.out.println("URL of active window is "+us);
		driver.quit();
	}

}
