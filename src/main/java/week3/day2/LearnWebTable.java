package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWebTable {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		Thread.sleep(3000);
		driver.findElementByXPath("//input[@id='txtStationFrom']").clear();
		driver.findElementByXPath("//input[@id='txtStationFrom']").sendKeys("MAS",Keys.TAB);
		driver.findElementByXPath("//input[@id='txtStationTo']").clear();
		driver.findElementByXPath("//input[@id='txtStationTo']").sendKeys("SBC",Keys.TAB);
		
		boolean selected = driver.findElementByXPath("//label[@for='chkSelectDateOnly']").isSelected();
		if(selected)
		{
			driver.findElementByXPath("//label[@for='chkSelectDateOnly']").click();
		}
		Thread.sleep(3000);
		WebElement table=driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> tr=table.findElements(By.tagName("tr"));
		System.out.println(tr.size());
		WebElement firstRow=tr.get(0);
		List<WebElement> td=firstRow.findElements(By.tagName("td"));
		String tct=td.get(1).getText();
		System.out.println(tct);
		for (WebElement sec : td) {
			System.out.println(sec);
		}
		
		
	} 

}
