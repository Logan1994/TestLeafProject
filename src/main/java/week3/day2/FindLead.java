package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FindLead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id='username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id='password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@value='Login']").click();
		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
		driver.findElementByXPath("//a[text()='Leads']").click();
		driver.findElementByXPath("//ul[@class='shortcuts']/li[3]/a").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Loganathan");
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		WebDriverWait wt=new WebDriverWait(driver,10);
		wt.until(ExpectedConditions.elementToBeClickable(By.className("x-btn-text")));
		WebElement web=driver.findElementByClassName("x-btn-center");
		List<WebElement> tr1=web.findElements(By.tagName("td"));
		System.out.println(tr1.size());
		WebElement firstRow=tr1.get(0);
		List<WebElement> td=firstRow.findElements(By.tagName("td"));
		String tct=td.get(1).getText();
			
		
	}

}
