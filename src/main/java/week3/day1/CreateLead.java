package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver =new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Accenture");
		driver.findElementById("createLeadForm_firstName").sendKeys("Loganathan");
		driver.findElementById("createLeadForm_lastName").sendKeys("Ganesan");
		WebElement sr = driver.findElementById("createLeadForm_dataSourceId");
		Select st=new Select(sr);
		st.selectByVisibleText("Conference");
		WebElement sr1 = driver.findElementById("createLeadForm_marketingCampaignId");
		Select st1=new Select(sr1);
		st1.selectByValue("CATRQ_CARNDRIVER");
		List<WebElement> sc= st1.getOptions();
		for (WebElement eachOption : sc) {
			System.out.println(eachOption.getText());
		}
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Logan");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Ganesh");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Mr.Logan");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Logan Profile");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Information Technology");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("15000");
		WebElement wb=driver.findElementById("createLeadForm_industryEnumId");
		Select se=new Select(wb);
		se.selectByValue("IND_SOFTWARE");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("100");
		WebElement wb1= driver.findElementById("createLeadForm_ownershipEnumId");
		Select se1=new Select(wb1);
		se1.selectByVisibleText("Partnership");
		driver.findElementById("createLeadForm_sicCode").sendKeys("5891");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("NYSE:ACC");
		driver.findElementById("createLeadForm_description").sendKeys("");
		driver.findElementById("createLeadForm_importantNote").sendKeys("");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("9655948285");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Logan");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("#333");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("loganathan656@gmail.com");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.myurl.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("TestLeaf");
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Test Leaf Lead");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("5 KVK Nagar");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Selaiyur");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		WebElement country= driver.findElementById("createLeadForm_generalCountryGeoId");
		Select sco=new Select(country);
		sco.selectByValue("IND");
		Thread.sleep(3000);
		WebElement state=driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select sct=new Select(state);
		sct.selectByVisibleText("TAMILNADU");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("600073");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("12345");
		driver.findElementByClassName("smallSubmit").click();
	}

}
