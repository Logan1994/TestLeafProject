package testcases;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods{
	@BeforeClass
	public void data()
	{
		testCaseId="TC003_DeleteLead";
		testDescription="Delete Lead";
		author="Logan";
		category="Smoke";
		

	}
//	@Test(dependsOnMethods="testcases.TC001_CreateLead.Step2")
	@Test(groups="regression")
	public void step4()
	{

		WebElement crm = locateElement("LinkText", "CRM/SFA");
		clickWithNoSnap(crm);
		WebElement fl = locateElement("Xpath", "//a[text()='Leads']");
		click(fl);
		WebElement sl = locateElement("Xpath", "//ul[@class='shortcuts']/li[3]/a");
		click(sl);
		WebElement phne = locateElement("Xpath", "//span[text()='Phone']");
		click(phne);
		WebElement phcode = locateElement("Xpath", "//input[@name='phoneCountryCode']");
		phcode.clear();
		type(phcode, "1");
		WebElement phnum = locateElement("Xpath", "//input[@name='phoneNumber']");
		type(phnum, "9655948285");
		WebElement find = locateElement("Xpath", "//button[text()='Find Leads']");
		click(find);
		WebElement table=locateElement("Xpath","//table[@class='x-grid3-row-table']");
		List<WebElement> tr=table.findElements(By.tagName("tr"));
		System.out.println(tr.size());
		WebElement firstRow=tr.get(0);
		WebElement txt = locateElement("Xpath", "(//table[@class='x-grid3-row-table']//a)[1]");
		String text = txt.getText();
		System.out.println("First Entry Lead id "+text);
		click(txt);
		WebElement del = locateElement("LinkText", "Delete");
		click(del);
		WebElement sl2 = locateElement("Xpath", "//ul[@class='shortcuts']/li[3]/a");
		click(sl2);
		WebElement searchId = locateElement("Xpath", "//input[@name='id']");
		type(searchId, text);
		WebElement findAgain = locateElement("Xpath", "//button[text()='Find Leads']");
		click(findAgain);
		WebElement errorMsg = locateElement("Xpath", "//div[@class='x-toolbar x-small-editor']//div");
		errorMsg.getTagName();
		verifyExactText(errorMsg, "No records to display");
	}
}
