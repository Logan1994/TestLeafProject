package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC004_MergeLeads extends ProjectMethods {
	@BeforeClass
	public void data()
	{
		testCaseId="TC001_CreateLead";
		testDescription="Create Lead";
		author="Logan";
		category="Smoke";
		
	}
	@Test(enabled=false)
	public void merge()
	{
	
		WebElement crm = locateElement("LinkText", "CRM/SFA");
		clickWithNoSnap(crm);
		WebElement fl = locateElement("Xpath", "//a[text()='Leads']");
		click(fl);
		WebElement cl = locateElement("LinkText", "Merge Leads");
		click(cl);
		WebElement clck = locateElement("Xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(clck);
		switchToWindow(1);
		WebElement fname=locateElement("Class", "firstName");
				type(fname, "Logan");
		WebElement clck2 = locateElement("Xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(clck2);
		
					
	}
	
}
