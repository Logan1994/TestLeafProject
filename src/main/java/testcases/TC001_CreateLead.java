package testcases;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
public class TC001_CreateLead extends ProjectMethods {
@BeforeClass(groups="common")
public void data()
	{
		testCaseId="TC001_CreateLead";
		testDescription="Create Lead";
		author="Logan";
		category="Smoke";
		excelfilename="createlead";
		
	}
//@Test(invocationCount=2,invocationTimeOut=70000)
@Test(groups="smoke",dataProvider="fetchData")
public void Step2(String cname,String finame,String laname,String data,String mark,String ph,String mailid)
{
	WebElement crm = locateElement("LinkText", "CRM/SFA");
	clickWithNoSnap(crm);
	WebElement cl = locateElement("LinkText", "Create Lead");
	click(cl);
	WebElement cmpny = locateElement("Id", "createLeadForm_companyName");
	type(cmpny, cname);
	WebElement fname = locateElement("Id", "createLeadForm_firstName");
	type(fname, finame);
	WebElement lname = locateElement("Id", "createLeadForm_lastName");
	type(lname, laname);
	WebElement srce = locateElement("Id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(srce, data);
	WebElement mrk = locateElement("Id", "createLeadForm_marketingCampaignId");
	selectDropDownUsingText(mrk, mark);
	WebElement phone = locateElement("Id", "createLeadForm_primaryPhoneNumber");
	type(phone, ph);
	WebElement mail = locateElement("Id", "createLeadForm_primaryEmail");
	type(mail, mailid);
	WebElement button = locateElement("Class", "smallSubmit");
	click(button);
	WebElement fnameVerify = locateElement("Id", "viewLead_firstName_sp");
	verifyExactText(fnameVerify, "Logan");
	
	
	/*WebElement crm = locateElement("LinkText", "CRM/SFA");
	clickWithNoSnap(crm);
	WebElement cl = locateElement("LinkText", "Create Lead");
	click(cl);
	WebElement cmpny = locateElement("Id", "createLeadForm_companyName");
	type(cmpny, "TestLeaf");
	WebElement fname = locateElement("Id", "createLeadForm_firstName");
	type(fname, "Logan");
	WebElement lname = locateElement("Id", "createLeadForm_lastName");
	type(lname, "Ganesh");
	WebElement srce = locateElement("Id", "createLeadForm_dataSourceId");
	selectDropDownUsingText(srce, "Conference");
	WebElement mrk = locateElement("Id", "createLeadForm_marketingCampaignId");
	selectDropDownUsingText(mrk, "Automobile");
	WebElement phone = locateElement("Id", "createLeadForm_primaryPhoneNumber");
	type(phone, "9655948285");
	WebElement mail = locateElement("Id", "createLeadForm_primaryEmail");
	type(mail, "logan.ganesh@gmail.com");
	WebElement button = locateElement("Class", "smallSubmit");
	click(button);
	WebElement fnameVerify = locateElement("Id", "viewLead_firstName_sp");
	verifyExactText(fnameVerify, "Logan");
	*/	
	
}

}
