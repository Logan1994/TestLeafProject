package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnReadExcel {
	public static Object[][] ReadExcel(String filename) throws IOException
	{
		XSSFWorkbook wbook= new XSSFWorkbook("./data/"+filename+".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		Object[][] data=new Object[lastRowNum][lastCellNum];
		for (int j = 1; j <= lastRowNum ;j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < lastCellNum; i++) {
				XSSFCell cell = row.getCell(i);
				String stringCellValue = cell.getStringCellValue();
				data[j-1][i]=stringCellValue;
				System.out.println(stringCellValue);
			} 
		}
		wbook.close();
		return data;
	}
	
}
