package week5.day2;

import java.security.cert.PKIXRevocationChecker.Option;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TC001_Facebook extends ProjectMethod {
	@BeforeClass
	public void data()
	{
		testCaseId="TC001_CreateLead";
		testDescription="FaceBook_Like_Test Leaf";
		author="Logan,Santhana";
		category="Smoke";

	}
	@Test	
	public void Steps() throws InterruptedException
	{
		WebElement srchtxt=locateElement("Xpath", "//input[@data-testid='search_input']");
		type(srchtxt, "TestLeaf");
		WebElement srch=locateElement("Xpath", "//button[@data-testid='facebar_search_button']");
		click(srch);
		Thread.sleep(2000);
		WebElement txt= locateElement("Xpath", "(//a[@class='_2yez'])[1]/div");
		verifyExactText(txt, "TestLeaf");
		Thread.sleep(2000);
		WebElement lclike = locateElement("Xpath","(//div[text()='TestLeaf']//following::button)[1]");
		String tt = getText(lclike);
		System.out.println(tt);
		if(tt.equalsIgnoreCase("Like"))
		{
			click(lclike);
		}
		else
		{
			System.out.println("Page is already Liked");
		}
       WebElement lt=locateElement("Xpath", "(//a[@class='_2yez'])[1]/div");
       click(lt);
       verifyTitle("TestLeaf - Home");
       WebElement locateElement2 = locateElement("Xpath","(//img[@alt='Highlights info row image'])[1]/following::div[1]");
       String likes = getText(locateElement2);
       System.out.println("No of likes "+likes);
	}
}
