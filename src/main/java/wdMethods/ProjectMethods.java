package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

public class ProjectMethods extends SeMethods {
	
	@DataProvider(name="fetchData")
	public Object[][] getdata() throws IOException {
		
		Object[][] readExcel = week6.day2.LearnReadExcel.ReadExcel(excelfilename);
		return readExcel;
	}
	@BeforeMethod(groups="common")
	@Parameters({"browser","url","username","password"})
	public void step1(String browser, String url, String uname, String pwd) {
		startApp(browser, url);
		WebElement eleUserName = locateElement("Id", "username");
		type(eleUserName, uname);
		WebElement elePassword = locateElement("Id","password");
		type(elePassword, pwd);
		WebElement eleLogin = locateElement("Class","decorativeSubmit");
		click(eleLogin);
	}
	@AfterMethod(groups="common")
	public void closingBrowser()
	{
		closeAllBrowsers();
	}
}
